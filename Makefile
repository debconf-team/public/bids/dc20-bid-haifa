all: budget.csv budget.ods

%.ods: %.fods
	libreoffice --convert-to ods $<

%.csv: %.fods
	libreoffice --convert-to csv $<
	
%.csv: %.ods
	libreoffice --convert-to csv $<
	
